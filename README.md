# mylist
# ===========
Example project to use easy_admin bundle of Symfony. 
In this project the user can create items and categories and manage them in a easy_admin. 

## Technologies
- Symfony 4
- Doctrine
- easy_admin bundle
- mysql 5.7

## Steps to run it
- Git clone git clone ``` $ git clone https://bitbucket.org/JaimeJesusSerrano/mylist.git ```
- Go to project root folder  ``` $ cd .../mylist/ ```
- Composer install ``` $ composer install ``` and solve all installation dependencies

Add new DB user:
- CREATE USER 'db_user'@'localhost' IDENTIFIED BY '123456';
- GRANT ALL ON mylist.* TO 'db_user'@'localhost';
- FLUSH PRIVILEGES;

Apply migrations:
- php bin/console doctrine:migrations:migrate

And finally...
- start server ``` $ php bin/console server:start ```